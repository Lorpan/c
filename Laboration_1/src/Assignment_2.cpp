//-----------------
// Laboration 1, Assignment_2.cpp
// Program with both logical and syntactical errors
// Lorentz Grip 2018-11-06
//-----------------
// moste ha iosstream för att använda cout cin etc
#include <iostream>
using namespace std;

int main() {
// Variables and constants
int radius, circumference, area;
// . inte ,
const float PI = 3.14;
// Input the circles radius
cout << "Assign the circle's radius: ";
// >> inte =
cin >> radius;
// Algorithm to calculate circumference (2*PI*r) and area (PI*r*r)
// area och omkrets är bytta
area = PI * radius * radius;
//det är noga med stora och små bokstäver
circumference = 2 * PI * radius;
// Output of results
//koden kan inte delas mitt i en del
cout << "A circle with the radius " << radius << " has the circumference "
<< circumference << " and area " << area << endl;
// Validate x
int x;
cin >> x;
// borde ha {} för att vara tydlig men deta fungerar
if(x == 100)
cout << "x is equal to 100" << endl;
//inget ; för funktoner
if(x > 0)
cout << "x is larger than zero" << endl;
// moste ha break för att inte alla case ska göras
switch(x){
case 5 : cout << "x is equal to 5 " << endl;
break;
case 10 : cout << "x is equal to 10" << endl;
break;
default : cout << "x is neither 5 nor 10" << endl;
// måste stänga switch
}
return 0;
} // End main
